/**
 * SOCKET.IO SERVER EVENTS
 * -----------------------
 * put your socket events here
 */

module.exports = {

	'connection': function(socket) {
		var username = socket.session.user? socket.session.user.username : "expired user";
		console.log("["+ new Date().toTimeString() +"] "+ username +" connect to socket");

		// kalo app di restart, session bakal ke restart, jadi user mesti relogin
		socket.emit('check auth', {
			shouldRelogin: socket.session.user? false : true
		});
	},

	// untuk event selain on 'connection', object socket ada di 'this'
	'disconnect': function (data) {
		var username = this.session.user? this.session.user.username : "someone";

		console.log("["+ new Date().toTimeString() +"] "+ username +" disconnected!");
	},

	// buat user yang ada di halaman dashboard
	'subscribe.questions': function(data) {
		if(this.session.user) {
			console.log("["+ new Date().toTimeString() +"] "+ this.session.user.username + " subscribe timeline questions");

			// join socket ke room 'subscribe.questions'
			// jadi kalo ada yang nanya ke siapapun, kita tinggal push pertanyaannya ke socket yang ada di room ini
			this.join('subscribe.questions');
		}
	},

	// buat user yang login (di semua halaman)
	'subscribe.question.notif': function(data) {
		if(this.session.user && this.session.user.username) {
			console.log("["+ new Date().toTimeString() +"] "+ this.session.user.username + " subscribe question notification");

			// join socket ke room 'notify.questions.< username logged user >'
			// jadi kalo ada yang nanya ke dia, kita tinggal push pertanyaannya ke socket yang ada di room ini 
			this.join('notify.questions.'+ this.session.user.username);
		}
	},

	// buat user yang login (di semua halaman)
	'subscribe.answer.notif': function(data) {
		if(this.session.user && this.session.user.username) {
			console.log("["+ new Date().toTimeString() +"] "+ this.session.user.username + " subscribe answer notification");

			// join socket ke room 'notify.answers.< username logged user >'
			// jadi kalo ada yang jawab pertanyaan dia, kita tinggal push notifikasi ke socket client di room ini
			this.join('notify.answers.'+ this.session.user.username);
		}
	},

	// buat user yang ada di halaman pertanyaan spesifik user
	'subscribe.user.questions': function(data) {
		if(this.session.user && this.session.user.username) {
			var page_owner = data.username;
			console.log("["+ new Date().toTimeString() +"] "+ this.session.user.username + " subscribe questions for "+page_owner);

			// join socket ke room 'subscribe.questions.< username halaman user yang dibuka >'
			// jadi kalo ada yang nanya ke 'page_owner', kita tinggal push pertanyaan ke socket client di room ini
			this.join('subscribe.questions.'+page_owner);
		}
	},

	// buat user yang ada di halaman jawaban
	'subscribe.user.answers': function(data) {
		if(this.session.user && this.session.user.username) {
			console.log("["+ new Date().toTimeString() +"] "+ this.session.user.username + " subscribe answers");

			// join socket ke room 'subscribe.answers.< user yang login >'
			// jadi kalo ada yang jawab pertanyaan dia, kita tinggal push jawaban ke socket client di room ini
			this.join('subscribe.answers.'+ this.session.user.username);
		}
	},

	// buat user yang ada di halaman pertanyaan user
	'subscribe.user.questions.answer': function(data) {
		if(this.session.user && this.session.user.username) {
			var page_owner = data.username;
			console.log("["+ new Date().toTimeString() +"] "+ this.session.user.username + " subscribe questions answer in timeline "+page_owner);

			// join socket ke room 'subscribe.questions.answer.< username halaman user yang dibuka >'
			// jadi kalo ada yang jawab pertanyaan ke 'page_owner', kita tinggal push jawaban ke socket client di room ini
			this.join('subscribe.questions.answer.'+page_owner);
		}
	},

	// buat user yang ada di halaman dashboard
	'subscribe.questions.answer': function(data) {
		if(this.session.user && this.session.user.username) {
			console.log("["+ new Date().toTimeString() +"] "+ this.session.user.username + " subscribe questions answer in global timeline");

			// join socket ke room 'subscribe.questions.answer'
			// jadi kalo ada jawab, push jawaban ke user yang ada di halaman dashboard
			this.join('subscribe.questions.answer');
		}
	}

};