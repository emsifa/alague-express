
module.exports = {

	/* CONTOH ROUTE SEDERHANA */
	'GET / as index': function(req, res, next) {
		if(req.session.user) {
			User.find({'username': {'$ne': req.user.username}})
			.limit(5)
			.select('username avatar')
			.exec(function(err, users) {
				if(err) next(err);

				data = { users: users };
				res.render("theme/pages/dashboard", data);
			});
		} else {
			res.render("theme/pages/homepage");
		}

	},

	/* CONTOH ROUTE CONTROLLER */
	'GET /login': Controller.Session.pageLogin,
	'POST /login': Controller.Session.login,

	'GET /register': Controller.User.pageRegister,
	'POST /register': Controller.User.register,

	'GET /logout': Controller.Session.logout,

	/* CONTOH ROUTE MIDDLEWARE */
	'GET /u/:username': [
		Filter.routeLoggedUser, // << ini middleware, bisa lebih dari 1, kalo mau tau isinya, buka ./app/libraries/Filter.js
 		Controller.User.pageUser // << yang terakhir ini action
	],

	'GET /questions': [	Filter.routeLoggedUser, Controller.User.pageQuestions ],
	'GET /answers': [	Filter.routeLoggedUser, Controller.User.pageAnswers ],

	'POST /ask': [ Filter.routeLoggedUser, Filter.routeAjax, Controller.Question.ask ],
	'POST /answer/:id_question': [ Filter.routeLoggedUser, Filter.routeAjax, Controller.Question.answer ],	

	'GET /json/timeline' : [ Filter.routeLoggedUser, Filter.routeAjax, Controller.Question.jsonTimeline ],
	'GET /json/timeline/:username': [ Filter.routeLoggedUser, Filter.routeAjax, Controller.Question.jsonUserQuestions ],
	'GET /json/answers/:username': [ Filter.routeLoggedUser, Filter.routeAjax, Controller.Question.jsonUserAnswers ]

}