/* express-session config
 * ----------------------
 * for more configuration, you can see at:
 * https://github.com/expressjs/session#options
 */

module.exports = {

	// null, memory, redis or mongoose
	// for redis, we use connect-redis
	store: null,

	name: 'appsess',

	secret: 'y0uR 5eCr3t k3Y',
	
	resave: false,
	
	saveUninitialized: true,

	// connect-redis options if you use redis as storage
	// https://github.com/tj/connect-redis#options
	redis: {}

};