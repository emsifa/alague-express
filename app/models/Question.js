module.exports = {

	collection: "questions",

	schema: {
		userFrom: {
			type: String,
			required: true
		},
		userTo: {
			type: String,
			required: false
		},
		question: {
			type: String,
			required: true
		},
		askAt: {
			type: Date,
			default: Date.now
		},
	},

};