module.exports = {

	ask: function(req, res, next) {
		var userTo = req.body.userTo;
		var question = req.body.question;

		User.findByUsername(userTo, function(err, user) {
			if(err) return next(err);

			if(!user) {
				return res.status(404).json({error: 'User tidak ditemukan'});
			}

			var question_data = {
				userFrom: req.user.username,
				userTo: user.username,
				question: question
			};

			Question.create(question_data, function(err, question) {
				if(err) return next(err);

				// push notifikasi pertanyaan ke client ke user yang ditanya
				App.io.to('notify.questions.'+question.userTo).emit('notify question', question);

				// push pertanyaan ke client yang berada di halaman si user yang ditanya
				App.io.to('subscribe.questions.'+question.userTo).emit('ask', question);

				// push pertanyaan ke user yang berada di halaman dashboard
				App.io.to('subscribe.questions').emit('ask', question);

				return res.status(200).json(question);
			});
		});
	},

	answer: function(req, res, next) {
		var questionId = req.param('id_question');

		Question.findById(questionId, function(err, question) {
			if(err) return next(err);

			if(!question) {
				return res.status(404).json({error: 'Pertanyaan tidak ditemukan, mungkin sudah dihapus'});
			}

			// jika yang jawab bukan yang ditanya
			if(question.userTo != req.user.username) {
				return res.status(200).json({error: 'Nggak nanya lu.. :p'});
			}

			var answer_data = {
				answer: req.body.answer,
				answerAt: Date.now()
			};

			Question.findByIdAndUpdate(questionId, {$set: answer_data}, function(err, question) {
				if(err) return next(err);

				// kirim notifikasi ke yang nanya
				App.io.to('notify.answers.'+question.userFrom).emit('notify answer', question);

				// kirim jawaban ke user yang ada di halaman jawaban si yang nanya
				App.io.to('subscribe.answers.'+question.userFrom).emit('answer', question);

				// kirim jawaban ke user yang ada di halaman pertanyaan yang ditanya
				App.io.to('subscribe.questions.answer.'+question.userTo).emit('answer', question);

				// kirim jawaban ke user yang ada di halaman dashboard
				App.io.to('subscribe.questions.answer').emit('answer', question);

				// kirim jawaban ke user yang ada di halaman dashboard
				// App.io.to('subscribe.answers').emit('answer', question);

				return res.status(200).json(question);
			});
		});
	},

	jsonTimeline: function(req, res, next) {
		var criteria = {};
		var limit = 7;

		if(req.query.maxId) {
			criteria._id = {$lt: req.query.maxId};
		}

		Question.find(criteria)
		.limit(limit)
		.sort({_id: -1})
		.exec(function(err, questions) {
			if(err) return next(err);

			return res.status(200).json({
				status: 'ok',
				questions: questions
			});
		});
	},

	jsonUserQuestions: function(req, res, next) {

		var username = req.param('username');
		var limit = 7;

		User.findByUsername(username, function(err, user) {
			if(err) return next(err);
			
			if(!user) {
				res.status(404).json({error: 'User not found'});
			}

			var criteria = {
				userTo: username
			};

			if(req.query.maxId) {
				criteria._id = {$lt: req.query.maxId};
			}

			Question.find(criteria)
			.limit(limit)
			.sort({_id: -1})
			.exec(function(err, questions) {
				if(err) return next(err);

				return res.status(200).json({
					status: 'ok',
					questions: questions
				});
			});
		});

	},

	jsonUserAnswers: function(req, res, next) {
		var username = req.user.username;
		var limit = 7;

		var criteria = {
			userFrom: username,
			answer: {
				$exists: 1
			}
		};

		Question.find(criteria)
		.limit(limit)
		.sort({_id: -1})
		.exec(function(err, questions) {
			if(err) return next(err);

			return res.status(200).json({
				status: 'ok',
				questions: questions
			});
		});
	}

};