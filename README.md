# Alague-Express (Testapp-1)
![Picture Alague-Exp](http://i1087.photobucket.com/albums/j474/Zulfindra_Juliant/Screenshot-Homepage-FirefoxDeveloperEdition-1_zps3144c452.png)

Source code test app pertama dari MVC framework Alague (alpha). 
Aplikasi ini bisa dibilang versi super simple dari ask.fm, dimana pada test app ini lebih sekitar 60% ditekankan pada penggunaan socket.io,
sedangkan 40% sisanya pengujian routes, event handler, mongoose pada model, jade view engine, dan beberapa fitur yang mendukung arsitektur MVC lainnya.

#### Video demonstrasi 
[youtube.com/watch?v=tmVWy6Fpyj8](https://www.youtube.com/watch?v=tmVWy6Fpyj8)

## Requirement
- Node.js (tested on v0.10.33)
- MongoDB (tested on v2.6.5)

## Persiapan Sebelumnya
- install git :
    - ubuntu (linux) : `sudo apt-get install git`
    - windows : Download [GitBash](http://git-scm.com/downloads)
- install Node.js :
    - ubuntu (linux) : `sudo apt-get install nodejs nodejs-legacy g++ curl libssl-dev apache2-utils` , jika sudah terinstall pasti cek ketik `node -v`
    - windows : download installernya [NodeJs](http://nodejs.org/download/)
- install MongoDb :
    - cara install disini [Cara Install MongoDb All OS](http://docs.mongodb.org/manual/tutorial/)
    - di ubuntu (linux) cek port mongodbnya di `/etc/mongod.conf`, pastikan port running di 27017

## Instalasi Alague-Express Testapp-1
- clone repo ini dengan cara `git clone -b testapp-1 --single-branch https://emsifa@bitbucket.org/emsifa/alague-express.git`
- pada terminal, masuk ke direktori alague-express
- di direktori tersebut, ketik `npm install`

## Test running
- Pastikan mongodb running pada port 27017, atau jika ingin mengubah port, dsb bisa disesuaikan di `app/configs/database.js`
- Jalankan `node server.js`

## Credits
by [Codedock](https://bitbucket.org/codedock)