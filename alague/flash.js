var _ = require("lodash");

module.exports = function(req, res, next) {
  req._flash = res.locals.flash = _.clone(req.session.flash, true);
  req.session.flash = {};

  // get flash
  req.flash = function(key) {
   return req._flash[key];
  };

  // set flash
  res.flash = function(key, value) {
   req.session.flash[key] = value;
  };

  next();
}