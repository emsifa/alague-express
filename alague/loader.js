var _ = require("lodash"),
    fs = require("fs"),
    Router = require("./router.js"),
    DbConnector = require("./db-connector.js"),
    debug = require("debug")("alague:loader");

// share express session to socket server :
// http://stackoverflow.com/questions/24679046/expressjssocket-ioexpress-session
function initializeSocketSession(socket, next) {
  var COOKIE_NAME = App.config.get("session.name");
  var COOKIE_SECRET = App.config.get("session.secret");
  var cookie = require("cookie");
  var cookieParser = require("cookie-parser");
  var sessionStore = App.sessionStore;

  try {
    var data = socket.handshake || socket.request;
    
    if (! data.headers.cookie) {
      return next(new Error('Missing cookie headers'));
    }

    var cookies = cookie.parse(data.headers.cookie);

    if (! cookies[COOKIE_NAME]) {
      return next(new Error('Missing cookie ' + COOKIE_NAME));
    }
    
    var sid = cookieParser.signedCookie(cookies[COOKIE_NAME], COOKIE_SECRET);
    
    if (! sid) {
      return next(new Error('Cookie signature is not valid'));
    }

    data.sid = sid;

    App.sessionStore.get(sid, function(err, session) {
      if (err) return next(err);

      //console.log(session);

      socket.session = session || {};
      next();
    });
  } catch (err) {
    next(new Error('Internal server error'));
  }
}

module.exports = {

  // LOAD CONTROLLERS
  loadControllers: function(app) {
    var dir = app.__configs.starter.controllers;
    var files = fs.readdirSync(dir);
    var controllers = {};

    files.forEach(function(file) {
      if(file.match(/\.js$/)) {
        var ctrl_path = dir+'/'+file;
        var ctrl_name = file.replace(/\.js$/, '');

        debug("Load controller "+ctrl_name);
        controllers[ctrl_name] = require(ctrl_path);
      }
    });

    global.Controller = controllers;
    return controllers;
  },

  // LOAD HELPERS
  loadHelpers: function(app) {
    var dir = app.__configs.starter.helpers;
    var files = fs.readdirSync(dir);
    var helpers = {};

    files.forEach(function(file) {
      if(file.match(/\.js$/)) {
        var helper_path = dir+'/'+file;
        var helper_name = file.replace(/\.js$/, '');

        debug("Load helper "+helper_name);
        global[helper_name] = helpers[helper_name] = require(helper_path);
      }
    });

    return helpers;
  },

  // LOAD ROUTES
  loadRoutes: function(app) {
    var router = new Router(app);
    var routes = app.config.load("routes");

    for(path in routes) {
      var route = new Router.Route(path, routes[path]);
      router.register(route);
    }

    return router;
  },

  // LOAD DATABASE CONNECTIONS
  loadDatabaseConnections: function(app, callback) {
    var dbConnector = new DbConnector(app);
    var default_connection = app.config.get("database.default_connection", "default");

    dbConnector
    .setDefaultConnection(default_connection)
    .createAppConnections()
    .then(function() {
      callback(dbConnector);
    });
  },

  // LOAD SOCKETS
  loadSocket: function(app){
    var io = require('socket.io')(app.server);

    if(app.config.get("app.session")) {
      io.use(initializeSocketSession);
    }

    var configs_socket = app.config.load("sockets");

    io.on('connection', function(socket) {

      if(typeof configs_socket.connection == 'function') {
        configs_socket.connection.apply(null, [socket]);
      }

      for(evt in configs_socket) {
        if(evt == 'connection') continue;

        debug('Register socket event '+evt);
        socket.on(evt, configs_socket[evt]);
      }

    });

    return io;
  },


  // LOAD MODELS
  loadModels: function(app) {
    var dir = app.__configs.starter.models;
    var files = fs.readdirSync(dir);
    var models = {};

    for(i in files) {
      var file = files[i];
      
      if(!/\.js$/.test(file)) continue;

      var model_path = dir+'/'+file;
      var model_name = file.replace(/\.js$/, '');
      var model_data = require(model_path);

      var default_connection = app.connection.__default_connection;

      model_data = _.extend({
        // schema options
        collection: undefined,
        strict: false, 
        bufferCommands: true,
        capped: false,
        versionKey: '__v',
        discriminatorKey: '__t',
        minimize: true,
        autoIndex: true,
        shardKey: null,
        read: null,
        id: true,
        _id: true,
        toObject: undefined,
        toJSON: undefined,

        // schema pre/post defined
        connection: default_connection,
        schema: {},
        methods: null,
        statics: null,
        beforeSave: null,
        afterSave: null,
        beforeValidate: null,
        afterValidate: null,
      }, model_data);

      var connection = app.connection.getConnection(model_data.connection);

      if(!connection) {
        throw new Error("Trying to create model in undefined connection "+model_data.connection+", maybe the name already used by your helper :\\");
      }

      var model_schema = model_data.schema;

      var model_schema_options = {
        collection: model_data.collection,
        strict: model_data.strict, 
        bufferCommands: model_data.bufferCommands,
        capped: model_data.capped,
        versionKey: model_data.versionKey,
        discriminatorKey: model_data.discriminatorKey,
        minimize: model_data.minimize,
        autoIndex: model_data.autoIndex,
        shardKey: model_data.shardKey,
        read: model_data.read,
        id: model_data.id,
        _id: model_data._id,
        toObject: model_data.toObject,
        toJSON: model_data.toJSON,
      };

      model_schema = app.mongoose.Schema(model_schema, model_schema_options);

      if(global[model_name] !== undefined) {
        throw new Error("Cannot initialize model "+model_name+" : object name already used, use another name");
      } else {

        if(model_data.methods) {
          model_schema.methods = model_data.methods;
        }

        if(model_data.statics) {
          model_schema.statics = model_data.statics;
        }

        if(typeof model_data.beforeValidate == "function") {
          model_schema.pre('validate', model_data.beforeValidate);
        }

        if(typeof model_data.afterValidate == "function") {
          model_schema.post('validate', model_data.afterValidate);
        }

        if(typeof model_data.beforeSave == "function") {
          model_schema.pre('save', model_data.beforeSave);
        }

        if(typeof model_data.afterSave == "function") {
          model_schema.post('save', model_data.afterSave);
        }

        debug("Load model "+model_name);
        global[model_name] = models[model_name] = connection.model(model_name, model_schema);
      }
    } // end for

    return models;
  },

};